# MediaTranscodePipeline

Pipeline For Transcoding Media

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
 __  __          _ _       
|  \/  | ___  __| (_) __ _ 
| |\/| |/ _ \/ _` | |/ _` |
| |  | |  __/ (_| | | (_| |
|_|  |_|\___|\__,_|_|\__,_|
 _____                                  _      
|_   _| __ __ _ _ __  ___  ___ ___   __| | ___ 
  | || '__/ _` | '_ \/ __|/ __/ _ \ / _` |/ _ \
  | || | | (_| | | | \__ \ (_| (_) | (_| |  __/
  |_||_|  \__,_|_| |_|___/\___\___/ \__,_|\___|
 ____  _            _ _            
|  _ \(_)_ __   ___| (_)_ __   ___ 
| |_) | | '_ \ / _ \ | | '_ \ / _ \
|  __/| | |_) |  __/ | | | | |  __/
|_|   |_| .__/ \___|_|_|_| |_|\___|
        |_|                        

Pipeline For Transcoding Media.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

MediaTranscodePipeline is licensed under the AGPLv3 license. Don't like it? Go else where.

## Donate
Like MediaTranscodePipeline? You use it yourself, or for your infrastructure? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59


## But How Do I Use It!?

This has 4 stages in the pipeline:

1. **WatchDog** (*MTPWatchDog*) - Watches for files moved into the directory you run this script. Adds file to MOVIE queue on PodMQ.
2. **Transcode** (*MTPTranscode*) - Pops movie title off of MOVIE queue, and transcodes, and copies it over to file server. Adds file to BACKUP queue on PodMQ.
3. **Backup** (*MTPBackup*) - Pops movie off of BACKUP queue, and backs up the file to Amazon S3 storage. Adds file to DELETE queue on PodMQ.
4. **Delete** (*MTPDelete*) - File has reached the end of the pipeline, and deletes movies. By this stage, movie is copied to file server, and backed up to S3.

Be sure to run all scripts in the folder you are moving videos to be transcoded to.
